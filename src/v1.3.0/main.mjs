export function setup(ctx) {

	// Mastery Spend Menu
	ctx.patch(SpendMasteryMenuItemElement, 'updateProgress').after(function(result, skill, action, spendAmount) {
		const xp = skill.getMasteryXP(action);
		const lvl = exp.xpToLevel(xp);
		this.level.textContent = `${ lvl }`;

		this.level.classList.remove('text-warning', 'text-success');
		this.progressBar.classList.remove('bg-warning', 'bg-success');
		this.progressBar.classList.add('bg-info');

		if (lvl >= 99) {
			this.level.classList.add('text-warning');
			this.progressBar.classList.replace('bg-info', 'bg-warning');
			showElement(this.xpRequired);
			this.xpRequired.textContent = `${ numberWithCommas(Math.floor(xp)) } XP`;
		}
		if (lvl >= 120) {
			this.level.classList.replace('text-warning', 'text-success');
			this.progressBar.classList.replace('bg-warning', 'bg-success');
		}
	});

	// Normal Mastery Display
	ctx.patch(MasteryDisplayElement, 'updateValues').after(function(result, progress) {
		const lvl = exp.xpToLevel(progress.xp);
		this.level.textContent = `${ lvl }`;

		this.level.classList.remove('text-warning', 'text-success');
		this.progressBar.classList.remove('bg-warning', 'bg-success');
		this.progressBar.classList.add('bg-info');

		if (lvl >= 99) {
			this.level.classList.add('text-warning');
			this.progressBar.classList.replace('bg-info', 'bg-warning');
		}
		if (lvl >= 120) {
			this.level.classList.replace('text-warning', 'text-success');
			this.progressBar.classList.replace('bg-warning', 'bg-success');
		}
	});

	// Compact Mastery Display
	ctx.patch(CompactMasteryDisplayElement, 'updateValues').after(function(result, progress) {
		const lvl = exp.xpToLevel(progress.xp);
		this.level.textContent = `${ lvl }`;

		this.level.classList.remove('text-warning', 'text-success');

		if (lvl >= 99) {
			this.level.classList.add('text-warning');
		}
		if (lvl >= 120) {
			this.level.classList.replace('text-warning', 'text-success');
		}
	});

	// Cooking Selection Mastery Display
	ctx.patch(CookingRecipeSelectionElement, 'updateMastery').after(function(result, recipe, cooking) {
		if (recipe.hasMastery) {
			const progress = cooking.getMasteryProgress(recipe);
			const lvl = exp.xpToLevel(progress.xp);
			this.masteryLevel.textContent = `${ lvl }`;

			this.masteryLevel.classList.remove('text-warning', 'text-success');

			if (lvl >= 99) {
				this.masteryLevel.classList.add('text-warning');
			}
			if (lvl >= 120) {
				this.masteryLevel.classList.replace('text-warning', 'text-success');
			}
		}
	});
}
